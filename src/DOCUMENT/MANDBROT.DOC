                        Chaos, Fractals and the Atari ST
                                  Support Disk

              Documentation for the Mandelbrot/Julia Set Generator

                             (c)1991 Conrad Bessant

Starting the program
--------------------
To start the program you should insert your back-up of the support disk in
the disk drive and double click on the relevant drive icon to display the
contents of the disk. Click on the MANDBROT.PRG icon to load the program.

Summary of the program
----------------------
The Mandelbrot set is now the recognised logo of Chaos theory, and there are
now many programs around which will render colourful Mandelbrot images on the
ST. The program supplied on this disk provides all the standard Mandelbrot
plotting and zooming facilities, with additional options and mathematical
information. A vast number of Julia sets may also be created from this program.

The program's main features are:

     o Generation of the Mandelbrot set or a Julia set with user-defined
       complex constant
     o Three plotting methods; colour (low resolution mode only), monochrome
       and 'set only'
     o Zoom facility with automatic aspect ratio preservation and iteration
       ceiling calculation
     o Filing of pictures and constants in Degas compatible format
     o Optional user-defined iteration ceiling
     o Display of constants when relevant

The program shares many of it's features with the Feigenbaum explorer described
in FEIGBAUM.DOC, if you have already used the Feigenbaum program then you
should be able to get accustomed to MANDBROT.PRG very quickly.

As with the Feigenbaum explorer, most of the menu options will be disabled
(shown in light text) when the program is loaded because there is no image on
the screen to manipulate. Before continuing it is therefore necessary to get a
Mandelbrot or Julia set onto the screen. This can be done either by plotting a
new set from scratch or by loading a previously drawn one.

Plotting a Mandelbrot or Julia set
----------------------------------
The similarities between Mandelbrot and Julia sets made it seem eminently
sensible to integrate the two fractals into one program. This has had the
effect of making many actions, such as file manipulation and zooming in, the
same for both fractals.

Before plotting a set you should first select the method of plotting from the
'colours' menu. The plotting methods are:

     o mono: the set will be rendered in alternate black and white contours
     o colour: each pixel will be plotted in a colour representing the
       number of iterations required to free that point from the 'Mandelbrot
       circle' (colour monitors only)
     o set only: only true members of the set will be plotted, in black on a
       white background

Note that only one of the 'colours' menu options may be selected at a time.

Plotting a Mandelbrot set
-------------------------
Once the plotting method has been selected the 'plot Mandelbrot' option of the
'Picture' menu can be used to plot the set. Before plotting begins a dialogue
window will appear in the bottom left corner of the screen informing you of the
range of x and y and prompting you to enter the iteration ceiling (max
iteration). The iteration ceiling determines the maximum number of iterations
performed for any one pixel. The suggested value of 33 should suffice for the
initial (full set) Mandelbrot plot. After pressing <Return> to accept the
iteration ceiling, plotting will begin. With a low iteration ceiling of 33
plotting will take about 10 minutes (40 minutes on high resolution monitors for
reasons explained in the book).

Plotting a Julia set
--------------------
Plotting a Julia set is very similar to plotting a Mandelbrot set except that
'Plot Julia set...' should be selected from the 'Picture' menu instead of the
Mandelbrot equivalent. Because there are an infinite number of Julia sets
compared to the single Mandelbrot set it is also necessary to select the
required set when choosing the Julia menu option. This is done by entering the
real and imaginary parts of the complex constant associated with the required
set. This constant is entered in the dialogue window which appears immediately
after selecting 'plot Julia set...' from the 'picture' menu. After selecting
the constant a dialogue window will appear, reminding you of the constant and
prompting you for the iteration ceiling. The iteration ceiling and plotting
duration guidlines given above for the Mandelbrot set also apply here.

Zooming in
----------
The method of magnifying a section of a set (i.e. zooming in) is identical for
Mandelbrot and Julia sets, and is very similar to the procedure used in the
Feigenbaum explorer.

Firstly the 'zoom in' option should be selected from the 'picture' menu, you
will then be prompted to select the section to magnify using the mouse. You can
only magnify rectangular sections, so you must first move to the top left
corner of the rectangle which you wish the enlarge. When at this position you
should press and HOLD DOWN the left mouse button, dragging out the resulting
rectangle until it encloses the desired area. Once the rectangle is
satisfactorily placed the left mouse button should be released (if you are not
satisfied with the rectangle the right mouse button should be pressed to cancel
the operation).

Once the rectangle has been selected a dialogue window will appear showing the
newly selected ranges of x and y, and suggesting an automatically calculated
iteration ceiling (based on the one that you entered for the previous plot).
After you have been given a chance to edit this ceiling the new section of the
set will start being plotted.

Notes:
     o It is possible to change the selections made in the 'colours' menu
       before zooming in
     o It is possible to zoom into a plot of an enlarged section
     o Note that it is possible to halt plotting (by holding down the right
       hand mouse button) and then zoom into the unfinished plot

Printing
--------
As in Feigenbaum explorer, this is only possible if you have copied a Degas
compatible printer driver into the root directory of your support disk back-up,
as described in README.DOC.

File operations
---------------
Mandelbrot and Julia sets can be saved and loaded from the 'file' menu using
the same method as in the Feigenbaum program. Note that the image currently on
display has already been saved to disk if a tick is visible against the 'save'
option in the 'file' menu. The files saved can be loaded into programs can read
Degas compatible files.

Interesting constants for Julia sets (from the book)
----------------------------------------------------
    Real part    Imaginary Part
     -1.16          -0.25
      0.32           0.04
     -1.25          -0.01
      0.00          -1.00

Interesting areas for enlargement are hard to convey here, but the spirals in
the first example shown above are very impressive.
