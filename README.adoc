= Computers and Chaos - Atari ST Edition

This is the digital version of the book `Computers and Chaos - Atari ST Edition`

*Author:* Conrad Bessant

*Year:* 1992

*Publisher:* Sigma Press


Released under *Creative Commons* 2018

It was published in two different versions, one for the Amiga platform and one for the Atari ST-platform.

== Description

_Chaos theory has found applications in areas as diverse as population dynamics, computer graphics and weather forecasting.
The related topic of fractals gives rise to computer-generated pictures of exquisite depth and beauty, resembling landscapes, trees and flowers.
Conrad Bessant explains the necessary mathematics in the simplest manner, with complete BASIC listings._

== Read this book: 

* https://gitlab.com/amigasourcecodepreservation/computers-and-chaos-atari-st-edition/blob/master/pdf/computers-and-chaos-atari-st-1992-bessant.pdf[Scanned pdf]

You can also find the source code examples in the git repository

== Contributing

If you'd like to help out, here is a suggested to-do:

* Convert the original scan to AsciiDoc - in progress for the amiga version.
* Add the Atari ST code (contact me, I have it)
* Proofread the Asciidoc text.
* Format all code examples that are not formated yet.
* Create the index.
* Add more horisontal bars, like in the original book
* Create more cross-references.
* Beautify the book more - minor details, look at other books generated from other AsciiDoc books.
* Make it autobuild an nice looking pdf, epub on checkin to master
* The docker container used for builds could be optimized a lot.

Also, there are some non-direct bugs regarding AsciiDoc and its framework that greatly would help this and future AsciiDoc books if they were resolved.

* https://github.com/asciidoctor/asciidoctor-pdf/issues/38[Sane content breaks with PDF generation]
* https://github.com/asciidoctor/asciidoctor/issues/857[Allow Image Title to Be Centered with Image]
* https://github.com/asciidoctor/asciidoctor/issues/450[Output Index when generating html from AsciiDoctor]
* https://github.com/asciidocfx/AsciidocFX/issues/282[Support for easy styling of output for PDF etc in AsciiDocFx without having to hack the docbook.xsl]
* https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=asciidoc[Much better support for AsciiDoc in GitLab]
* https://github.com/jneen/rouge/wiki/List-of-supported-languages-and-lexers[Add GFA BASIC support to Rouge syntax highlighter]
 
== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

Many thanks to Conrad Bessant for letting his Amiga & Atari books becoming available in digital form, and for being so supportive of making it happen.

== Copyright & Author

Conrad Bessant © 1992 - 2018


